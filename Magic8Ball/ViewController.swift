//
//  ViewController.swift
//  Magic8Ball
//
//  Created by James Robert Sawley on 18/07/2018.
//  Copyright © 2018 James Robert Sawley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let ballImages = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    var ballIndex : Int = 0
    
    @IBOutlet weak var eightBallImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getRandomBallImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func askQuestionButton(_ sender: Any) {
        
        getRandomBallImage()
        
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        getRandomBallImage()
        
    }
    
    func getRandomBallImage() {
        ballIndex = Int(arc4random_uniform(5))
        eightBallImage.image = UIImage(named: ballImages[ballIndex])
    }
    
}

